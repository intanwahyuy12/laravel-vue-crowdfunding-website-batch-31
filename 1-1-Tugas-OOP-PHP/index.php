<?php

abstract class Hewan {
  protected $nama;
  protected $darah = 50;
  protected $jumlahKaki;
  protected $keahlian;
 
  


  function atraksi($nama){
      return $this->nama=$nama ." sedang ".$this->keahlian;
  }

}

abstract class Fight extends Hewan {
  public $attackPower;
  public $defencePower;
  
  public function __construct($jumlahKaki, $keahlian, $attackPower, $defencePower){
    $this->$jumlahKaki = $jumlahKaki;
    $this->$keahlian = $keahlian;
    $this->$attackPower = $attackPower;
    $this->$defencePower = $defencePower;

  }
  

  function serang($penyerang, $objekSerang){
    return $penyerang. "sedang menyerang". $objekSerang;

  }

  function diserang($penyerang, $objekSerang){
    return $objekSerang. " sedang diserang " .$penyerang;
  }
}


class Elang extends Fight{
  
  function getInfoHewan(){
    
  }

}

class Harimau extends Fight{
  function getInfoHewan(){
    
  }
}


$elang = new Elang(2,"terbang tinggi", 10 , 5);
$harimau = new Harimau(4,"lari cepat", 7, 8);

echo $elang->atraksi('Elang_1');


?>